const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const devMode = process.env.NODE_ENV !== "production";
console.log('devMode', devMode);

module.exports = {
    entry: "./src/index.js",
    mode: "development",
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                    {
                        loader: "ts-loader",
                        options: {
                            compilerOptions: {
                                noEmit: false,
                            },
                        },
                    }
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: { presets: ["@babel/env"] }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.svg$/,
                use: ['@svgr/webpack'],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    devMode ? "style-loader" : MiniCssExtractPlugin.loader,
                    {
                        // Translates CSS into CommonJS
                        loader: 'css-loader',
                        options: {
                            modules: {
                                auto: true,
                                localIdentName: '[name]__[local]___[hash:base64:5]',
                            },
                        },
                    },
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
        ]
    },
    resolve: { extensions: ["*", ".js", ".jsx", ".ts", ".tsx"] },
    output: {
        path: path.resolve(__dirname, "dist/"),
        publicPath: "/",
        filename: "[hash]_bundle.js"
    },
    devServer: {
        static: {
            directory: path.join(__dirname, "public"),
            watch: true,
        },
        client: {
            logging: 'verbose',
            overlay: true,
            progress: true,
        },
        port: 3000,
        hot: false,
        liveReload: true,
        watchFiles: {
            paths: ['src/**/*.js', 'src/**/*.jsx', 'src/**/*.ts', 'src/**/*.tsx', 'src/**/*.scss', 'public/*'],
            options: {
                usePolling: false,
                useFsEvents: true,
                
            },
        },
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Lada',
            template: 'index.template',
        }),
        new MiniCssExtractPlugin({
            filename: devMode ? "[name].css" : "[name]_[contenthash].css",
            chunkFilename: devMode ? "[name].css" : "[name]_[contenthash].css",
        }),
    ],
    // plugins: [new webpack.HotModuleReplacementPlugin()]
};
