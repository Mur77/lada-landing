import React from "react"

import { CommonLayout } from "./layouts/commonLayout/CommonLayout"
import { Home } from './views/home/Home'

import "./styles/globals.scss"

// import 'swiper/swiper.scss'
// import 'swiper/components/navigation/navigation.scss'
// import 'swiper/components/pagination/pagination.scss'

class App extends React.Component {
    render() {
        return(
            <div className="App">
                <CommonLayout>
                    <Home />
                </CommonLayout>
            </div>
        )
    }
}

export default App
