import React from 'react'

import { Slider } from '../../components/slider/Slider'
import { SocialNetworks } from './components/socialNetworks/SocialNetworks'
import { OurFriends } from './components/ourFriends/OurFriends'

import { messages } from '../../messages/messages'

import styles from './Home.module.scss'

import { slider1 } from './data/slides-rig'
import { friends } from './data/friends'

export const Home: React.FC = () => {
    return (
        <div className={styles.container}>

            <div className={styles.section1}>
                <div className={styles.section1TitleContainer}>
                    <div className={styles.logo}>
                        <img className={styles.logoImage} src='images/lada-logo-small.png' alt='lada-logo' />
                    </div>
                    {messages.home.title}
                    <div className={styles.section1Text}>
                        {messages.home.text}
                    </div>
                    <SocialNetworks />
                </div>

                <div className={styles.section1SliderContainer}>
                    <Slider items={slider1} />
                </div>
            </div>

            <div className={styles.section2}>
                <OurFriends friends={friends} />
            </div>

        </div>
    )
}
