import { iFriend } from '../../../interfaces'

export const friends: iFriend[] = [
    {
        image: 'images/friends/english-centre.png',
        description: 'English centre',
        link: 'https://www.englishcentre.org/',
    },
    {
        image: 'images/friends/creative-society.jpg',
        description: 'Creative society',
        link: 'https://creativesociety.com/uk',
    },
]
