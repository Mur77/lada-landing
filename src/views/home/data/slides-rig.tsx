import { iHomeSlide } from '../../../interfaces'

export const slider1: iHomeSlide[] = [
    {
        id: '0',
        original: 'images/pictures/img0627.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '1',
        original: 'images/pictures/img2480.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '2',
        original: 'images/pictures/img2495.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '9',
        original: 'images/pictures/img2616.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '10',
        original: 'images/pictures/img2618.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '13',
        original: 'images/pictures/img2891.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '14',
        original: 'images/pictures/img2998.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '15',
        original: 'images/pictures/img3019.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '16',
        original: 'images/pictures/img3028.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '17',
        original: 'images/pictures/img3092.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '18',
        original: 'images/pictures/img3103.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '19',
        original: 'images/pictures/img6175.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '20',
        original: 'images/pictures/img6181.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '21',
        original: 'images/pictures/img6183.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '22',
        original: 'images/pictures/img6193.jpg',
        originalClass: 'sliderImageCustomClass',
    },
    {
        id: '23',
        original: 'images/pictures/img6194.jpg',
        originalClass: 'sliderImageCustomClass',
    },
]
