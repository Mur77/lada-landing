import React from "react";

import FacebookIcon from '@/../../public/images/icons/social/facebook.svg'
import InstagramIcon from '@/../../public/images/icons/social/instagram.svg'

import styles from './SocialNetworks.module.scss'

export const SocialNetworks: React.FC = () => {
    return (
        <div className={styles.container}>
            <a className={styles.icon} href="https://www.facebook.com/apsLada/" target="_blank">
                <FacebookIcon />
            </a>
            <a className={styles.icon} href="https://www.instagram.com/aps_lada/" target="_blank">
                <InstagramIcon />
            </a>
        </div>
    )
}
