import React from 'react'

import { Footer } from '../../components/footer/Footer'
import { Header } from '../../components/header/Header'

import styles from './CommonLayout.module.scss'

export const CommonLayout = ({ children }) => {
    return (
        <>
            <div className={styles.container}>
                <Header />
                {children}
            </div>
            <Footer />
        </>
    )
}