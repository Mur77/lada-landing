import React from 'react'
import ImageGallery from 'react-image-gallery'

import styles from './Slider.module.scss'
import "./image-gallery.scss"

import { iHomeSlide } from '../../interfaces'

interface iSlider {
    items: iHomeSlide[]
    infinite?: boolean
    showNav?: boolean
    showThumbnails?: boolean
    showFullscreenButton?: boolean
    showPlayButton?: boolean
    autoPlay?: boolean
    disableKeyDown?: boolean
    disableSwipe?: boolean
    slideInterval?: number
}

export const Slider: React.FC<iSlider> = ({ 
    items, 
}) => {
    const params: iSlider = {
        items: items,
        infinite: true,
        showNav: false,
        showThumbnails: false,
        showFullscreenButton: false,
        showPlayButton: false,
        autoPlay: true,
        disableKeyDown: true,
        disableSwipe: true,
        slideInterval: 5000,
    }

    return (
        <div className={styles.sliderContainer}>
            <ImageGallery {...params} />
        </div>
    )
}
