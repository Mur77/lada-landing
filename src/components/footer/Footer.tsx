import React from 'react'

import { messages } from '../../messages/messages'

import styles from './Footer.module.scss'

export const Footer: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <div className={styles.about}>{messages.footer.about}</div>
            </div>
        </div>
    )
}
