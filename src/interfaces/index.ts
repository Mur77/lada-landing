import { ReactElement } from "react"

interface iSubmenuItem {
    title: string
    link: string
}

export interface iMenuItem {
    title: string
    link: string
    submenu: iSubmenuItem[]
}

export interface iHomeSlide {
    id: string
    original: string
    text?: ReactElement | string
    description?: string
    originalClass?: string
}

export interface iAnimal {
    id: string
    image: string
    nickname: string
    description: string
    link: string
    quote: string
}

export interface iIntro {
    text: string
}

export interface iUser {
    id: string
    email: string
    image: string
    role: string
    name: string
    middlename: string
    surname: string
    position: string
    description: string
    team: boolean
}

export interface iFriend {
    image: string
    description: string
    link: string
}

export interface iWeekAnimal {
    title: string,
    image: string,
    description: string,
}
